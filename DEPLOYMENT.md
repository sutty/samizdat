# Deployment

***NOTICE: this is a work in progress, and is incomplete at this time.***

Samizdat deployment is currently mostly a manual process. It involves:
1. getting the code
1. creating a [Gun](https://gun.eco/) account to push content addresses to
1. replacing the Gun account public key in the `gun+ipfs` plugin code
1. deploying the code to your server

This is highly suboptimal; work is being done to design and implement a process that is more user-friendly and automated. In the meantime, you can deploy Samizdat by following these steps.

## 1. Getting the code

You can get the code from the git repository. You need:
 - everything in the `lib/` subdirectory
 - everything in the `plugins/` subdirectory
 - `samizdat.js`
 - `service-worker.js`

## 2. Creating a Gun account

This requires using your browser console to run a few lines of JavaScript. First, go to your `index.html` and load the Gun library:

```javascript
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'lib/gun.js';
document.head.appendChild(script);
```

Then, set up Gun:

```javascript
gun = Gun();
user = gun.user();
```

Finally, create the user:

```javascript
user.create('your-username', 'your-password' function(ack){ console.log(ack.pub) });
```

You should now see the public key displayed in the console. You will need this for the next step.

## 3. Replacing the Gun public key

In `plugins/gun-ipfs.js`, find the line that defines the `gunPubKey` constant and make sure it is defined as your new public key. It should look something like this:

```javascript
const gunPubKey = 'PydLOfkID_EPABjZ5vKoq25hrWdnREq8VSPRAvrpKIO.EjJtduhyPziWK0f7iILodu-EjGHli1tCZ4zCGGOWEow';
```

## 4. Deploying to the server

Place the files mentioned above into the root directory of your website. Finally, add the following line to your `index.html` (assuming `samizdat.js` is in the same directory):

```html
<script defer src="./samizdat.js"></script>
```
