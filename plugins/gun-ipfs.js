/**
 * this is the default Gun+IPFS strategy plugin
 * for Samizdat.
 * 
 * it uses Gun for content address resolution
 * and IPFS for delivery
 */


/* ========================================================================= *\
|* === General stuff and setup                                           === *|
\* ========================================================================= */

var ipfs;
var gun;
var gunUser;

// the pubkey of the preconfigured Gun user
const gunPubKey = 'WUK5ylwqqgUorceQRa84qfbBFhk7eNRDUoPbGK05SyE.-yohFhTzWPpDT-UDMuKGgemOUrw_cMMYWpy6plILqrg'

// the IPFS gateway e're using for verification
const IPFSGateway = 'https://gateway.ipfs.io'

/**
 * this is apparently needed by Gun
 * and `window` does not exist in ServiceWorker context
 */
if (typeof window === 'undefined') {
    console.log('(COMMIT_UNKNOWN) redefining window...')
    var window = self;
}

/**
 * importing stuff works differently between a browser window context
 * and a ServiceWorker context, because things just can't be easy and sane
 */
function doImport() {
    var args = Array.prototype.slice.call(arguments);
    if (typeof self.importScripts !== 'undefined') {
        self.importScripts.apply(self, args)
    } else {
        console.log('(COMMIT_UNKNOWN) assuming these scripts are already included:')
        args.forEach(function(src){
            console.log('+--', src)
        })
    }
}

async function setup_ipfs() {
    if (ipfs === undefined) {
        ipfs = false // we don't want to start a few times over
        console.log('(COMMIT_UNKNOWN) Importing IPFS-related libraries...');
        doImport(
            "./lib/ipfs.js");
        console.log('(COMMIT_UNKNOWN) Setting up IPFS...')
        ipfs = await self.Ipfs.create();
        console.log('+-- IPFS loaded       :: ipfs is   : ' + typeof ipfs)
    }
}

async function setup_gun() {
    if (gun === undefined) {
        gun = false // we don't want to start a few times over
        console.log('(COMMIT_UNKNOWN) Importing Gun-related libraries...');
        doImport(
            "./lib/gun.js",
            "./lib/sea.js",
            "./lib/webrtc.js");
        console.log('(COMMIT_UNKNOWN) Setting up Gun...')
        gun = Gun(['https://gunjs.herokuapp.com/gun', 'https://samizdat.is/gun']);
        console.log('+-- Gun loaded        :: gun is    : ' + typeof gun);
    }
    if ( (gun !== false) && (gun !== undefined) && (gunUser === undefined) ) {
        gunUser = false // we don't want to start a few times over
        console.log('(COMMIT_UNKNOWN) Setting up gunUser...')
        gunUser = gun.user(gunPubKey)
        console.log('+-- Gun init complete :: gunUser is: ' + typeof gunUser);
    }
}

async function setup_gun_ipfs() {
    if (ipfs === undefined || gun === undefined) {
        console.log('(COMMIT_UNKNOWN) Setting up Samizdat...')
        setup_ipfs();
        setup_gun();
    } else {
        console.log('(COMMIT_UNKNOWN) Samizdat setup already underway (ipfs: ' + ( (ipfs) ? 'done' : 'loading' ) + ', gun: ' + ( (gun) ? 'done' : 'loading' ) + ')')
    }
}


/* ========================================================================= *\
|* === Main functionality                                                === *|
\* ========================================================================= */

let getGunData = (gunaddr) => {
    return new Promise(
        (resolve, reject) => {
            console.log('(COMMIT_UNKNOWN) getGunData()');
            console.log('(COMMIT_UNKNOWN) getGunData() :: +-- gunUser   : ' + typeof gunUser);
            console.log('(COMMIT_UNKNOWN) getGunData() :: +-- gunaddr[] : ' + gunaddr);

            // get the data
            gunUser
                .get(gunaddr[0])
                .get(gunaddr[1])
                .once(function(addr){
                    if (typeof addr !== 'undefined') {
                        console.log("2.1 IPFS address: '" + addr + "'");
                        resolve(addr);
                    } else {
                        // looks like we didn't get anything
                        reject(new Error('IPFS address is undefined for: ' + gunaddr[1]))
                    }
                // ToDo: what happens when we hit the timeout here?
                }, {wait: 5000});
        }
    );
};

/**
 * the workhorse of this plugin
 */
async function getContentFromGunAndIPFS(url) {
    var urlArray = url.replace(/https?:\/\//, '').split('/')
    var gunaddr = [urlArray[0], '/' + urlArray.slice(1).join('/')]

    /*
     * if the gunaddr[1] ends in '/', append 'index.html' to it
     */
    if (gunaddr[1].charAt(gunaddr[1].length - 1) === '/') {
        console.log("NOTICE: address ends in '/', assuming '/index.html' should be appended.");    
        gunaddr[1] += 'index.html';
    }

    console.log("2. Starting Gun lookup of: '" + gunaddr.join(', ') + "'");
    console.log("   +-- gun     : " + typeof gun);
    console.log("   +-- gunUser : " + typeof gunUser);

    /*
     * naïvely assume content type based on file extension
     * TODO: this needs a fix
     */
    var contentType = '';
    switch (gunaddr.slice(-1)[0].split('.', -1)[1].toLowerCase()) {
        case 'html':
        case 'htm':
            contentType = 'text/html';
            break;
        case 'css':
            contentType = 'text/css';
            break;
        case 'js':
            contentType = 'text/javascript';
            break;
        case 'svg':
            contentType = 'image/svg+xml';
            break;
        case 'ico':
            contentType = 'image/x-icon';
            break;
    }
    console.log("   +-- guessed contentType : " + contentType);

    return getGunData(gunaddr).then(ipfsaddr => {
        console.log("3. Starting IPFS lookup of: '" + ipfsaddr + "'");
        return ipfs.get(ipfsaddr);
    }).then(files => {
        console.log('4. Got a Gun-addressed IPFS-stored file: ' + files[0].path + '; content is: ' + typeof files[0].content);

        // creating and populating the blob
        var blob = new Blob(
            [files[0].content],
            {'type': contentType}
        );

        return new Response(
            blob,
            {
                'status': 200,
                'statusText': 'OK',
                'headers': {
                    'Content-Type': contentType,
                    'ETag': files[0].path,
                    'X-Samizdat-Method': 'gun+ipfs',
                    'X-Samizdat-ETag': files[0].path
                }
            }
        );
    });
}


/* ========================================================================= *\
|* === Publishing stuff                                                  === *|
\* ========================================================================= */
/*
 * these are used for adding content to IPFS and Gun
 */


/**
 * adding stuff to IPFS
 * accepts an array of URLs
 * 
 * returns a Promise that resolves to an object mapping URLs to IPFS hashes
 */
let addToIPFS = (resources) => {
    return new Promise((resolve, reject) => {
        
        console.log("Adding to IPFS...")
        console.log("+-- number of resources:", resources.length)
        var ipfs_addresses = {};

        resources.forEach(function(res){
            console.log("    +-- handling internal resource:", res)
            
            ipfs.ready.then(ipfs => {
                // adding from URL, since it's easier than handling the downloading ourselves
                ipfs.addFromURL(res, (err, result) => {
                    if (err) {
                        reject(err)
                    }
                    // add to the list -- this is needed to add stuff to Gun
                    // result[0].path is just the filename stored in IPFS, not the actual path!
                    // res holds the full URL
                    // what we need in ipfs_addresses is in fact just the absolute path (no domain, no scheme)
                    var abs_path = res.replace(window.location.origin, '')
                    ipfs_addresses[abs_path] = '/ipfs/' + result[0].hash
                    console.log("Added to IPFS: " + abs_path + ' as ' + ipfs_addresses[abs_path])
                    // if we seem to have all we need, resolve!
                    if (Object.keys(ipfs_addresses).length === resources.length) resolve(ipfs_addresses);
                })
            })
        
        });
    })
}

/**
 * verification that content pushed to IPFS
 * is, in fact, available in IPFS
 * 
 * a nice side-effect is that this will pre-load the content on
 * a gateway, which tends to be a large (and fast) IPFS node
 * 
 * this is prety naïve, in that it pulls the content from an ipfs gateway
 * and assumes all is well if it get a HTTP 200 and any content
 * 
 * that is, it does *not* check that the content matches what was pushed
 * we trust IPFS here, I guess
 * 
 * finally, we're using a regular fetch() instead of just going through our
 * ipfs object because our IPFS object might have things cached and we want
 * to test a completey independent route
 * 
 * takes a object mapping paths to IPFS addresses
 * and returns a Promise that resolves to true 
 */
let verifyInIPFS = (ipfs_addresses) => {
    return new Promise((resolve, reject) => {
        console.log('Checking IPFS content against a gateway...')
        console.log('+-- gateway in use: ' + IPFSGateway)
        // get the list of IPFS addresses
        var updatedPaths = Object.values(ipfs_addresses)
        for (path in ipfs_addresses) {
            // start the fetch
            fetch(IPFSGateway + ipfs_addresses[path])
                .then((response) => {
                    ipfsaddr = response.url.replace(IPFSGateway, '')
                    if (response.ok) {
                        console.log('+-- verified: ' + ipfsaddr)
                        var pathIndex = updatedPaths.indexOf(ipfsaddr)
                        if (pathIndex > -1) {
                            updatedPaths.splice(pathIndex, 1)
                        }
                        if (updatedPaths.length === 0) {
                            console.log('All updates confirmed successful!')
                            resolve(ipfs_addresses);
                        }
                    } else {
                        reject(new Error('HTTP error (' + response.status + ' ' + response.statusText + ' for: ' + ipfsaddr))
                    }
                })
                .catch((err) => {
                    // it would be nice to have the failed path here somehow
                    // alternatively, updating updatedPaths with info on failed
                    // requests might work
                    reject(err)
                })
        }
    })
}

/**
 * auth a Gun admin user
 * (and verify it's the correct one with regards to the configured gunPubKey)
 */
let authGunAdmin = (user, pass) => {
    return new Promise((resolve, reject) => {
        // we need a separate Gun instance, otherwise gu will get merged with gunUser
        // and we want these to be separate
        var g = Gun(['https://gunjs.herokuapp.com/gun', 'https://samizdat.is/gun'])
        var gu = g.user()
        gu.auth(user, pass, (userReference) => {
            if (userReference.err) {
                reject(new Error(userReference.err))
            // reality check -- does it match our preconfigured pubkey?
            } else if (gu._.soul.slice(1) === gunPubKey) {
                console.log('Gun Admin user authenticated using password.');
                // we need to keep the reference to g, otherwise gu becomes unusable
                var gApi = {
                    user: gu,
                    gun: g
                } 
                resolve(gApi)
            } else {
                reject(new Error('Password-authenticated user does not match preconfigured pubkey!'))
            }
        })
    })
}

/**
 * add IPFS addresses to Gun
 */
let addToGun = (user, pass, ipfs_addresses) => {
    // we need an authenticated Gun user
    return authGunAdmin(user, pass)
        .then((gunAPI) => {
            console.log('+-- adding new IPFS addresses to Gun...')
            gunAPI.user.get(window.location.host).put(ipfs_addresses /*, function(ack) {...}*/);
            return gunAPI;
        })
        /**
         * regular confirmations don't seem to work
         * 
         * so instead we're using the regular read-only Gun user
         * to .get() the data that we've .put() just a minute ago
         * 
         * we then subscribe to the .on() events and once we notice the correct
         * addresseswe consider our job done and quit.
         */
        .then((gunAPI) => {
            // get the paths
            console.log('+-- starting verification of updated Gun data...')
            var updatedPaths = Object.keys(ipfs_addresses)
            for (path in ipfs_addresses) {
                console.log('    +-- watching: ' + path)
                //debuglog('watching path for updates:', path)
                // using the global gunUser to check if updates propagated
                gunUser.get(window.location.host).get(path).on(function(updaddr, updpath){
                    /*debuglog('+--', updpath)
                    debuglog('    updated  :', ipfs_addresses[updpath])
                    debuglog('    received :', updaddr)*/
                    if (ipfs_addresses[updpath] == updaddr) {
                        // update worked!
                        gunUser.get(window.location.host).get(updpath).off()
                        console.log('+-- update confirmed for:', updpath, '[' + updaddr + ']')
                        var pathIndex = updatedPaths.indexOf(updpath)
                        if (pathIndex > -1) {
                            updatedPaths.splice(pathIndex, 1)
                        }
                        if (updatedPaths.length === 0) {
                            console.log('All updates confirmed successful!')
                            return true;
                        }
                    }
                })
            }
        })
}

/**
 * example code for of adding content to IPFS, verifying it was successfully added,
 * and adding the new addresses to Gun (and verifying changes propagated) 
 * 
 * TODO: this should accept a URL, a Response, or a list of URLs,
 *       and handle stuff appropriately
 */
let publishContent = (resource, user, password) => {
    
    if (typeof resource === 'string') {
        // we need this as an array of strings
        resource = [resource]
    } else if (typeof resource === 'object') {
        if (!Array.isArray(resource)) {
            // TODO: this needs to be implemented such that the Response is used directly
            //       but that would require all called functions to also accept a Response
            //       and act accordingly; #ThisIsComplicated
            throw new Error("Handling a Response: not implemented yet")
        }
    } else {
        // everything else -- that's a paddlin'!
        throw new TypeError("Only accepts: string, Array of string, Response.")
    }
        
    // add to IPFS
    var ipfsPromise = addToIPFS(resource)
    return Promise.all([
        // verify stuff ended up in IPFS
        ipfsPromise.then(verifyInIPFS),
        // add to Gun and verify Gun updates propagation
        ipfsPromise.then((hashes) => {
            addToGun(user, password, hashes)
        })
    ])
}

/* ========================================================================= *\
|* === Initialization                                                    === *|
\* ========================================================================= */

// we probably need to handle this better
setup_gun_ipfs();

// initialize the SamizdatPlugins array
if (!Array.isArray(self.SamizdatPlugins)) {
    self.SamizdatPlugins = new Array()
}

// and add ourselves to it
// with some additional metadata
self.SamizdatPlugins.push({
    name: 'gun+ipfs',
    description: 'Decentralized resource fetching using Gun for address resolution and IPFS for content delivery.',
    version: 'COMMIT_UNKNOWN',
    fetch: getContentFromGunAndIPFS,
    publish: publishContent
})
