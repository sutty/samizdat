/* ========================================================================= *\
|* === Stashing plugin using the Cache API                               === *|
\* ========================================================================= */

/**
 * getting content from cache
 */
let getContentFromCache = (url) => {
    console.log('Samizdat: getting from cache!')
    return caches.open('v1')
        .then((cache) => {
            return cache.match(url)
        })
        .then((response) => {
            if (typeof response === 'undefined') {
                throw new Error('Resource not found in cache');
            } else {
                response.headers.forEach(function(v, k){
                    console.log('+-- Retrieved cached header: ', k, ' :: ', v)
                });
                // return the response
                return response
            }
        })
}

/**
 * add resources to cache
 * 
 * implements the stash() Samizdat plugin method
 * 
 * accepts either a Response
 * or a string containing a URL
 * or an Array of string URLs
 */
let cacheContent = (resource, key) => {
    return caches.open('v1')
        .then((cache) => {
            if (typeof resource === 'string') {
                // assume URL
                console.log("(COMMIT_UNKNOWN) caching an URL")
                return cache.add(resource)
            } else if (Array.isArray(resource)) {
                // assume array of URLs
                console.log("(COMMIT_UNKNOWN) caching an Array of URLs")
                return cache.addAll(resource)
            } else {
                // assume a Response
                // which means we either have a Request in key, a string URL in key,
                // or we can use the URL in resource.url
                if ( (typeof key !== 'object') && ( (typeof key !== 'string') || (key === '') ) ) {
                    if (typeof resource.url !== 'string' || resource.url === '') {
                        throw new Error('No URL to work with!')
                    }
                    key = resource.url
                }
            
                // we need to create a new Response object
                // with all the headers added explicitly
                // otherwise the x-samizdat-* headers get ignored
                var init = {
                    status:     resource.status,
                    statusText: resource.statusText,
                    headers: {}
                };
                resource.headers.forEach(function(val, header){
                    init.headers[header] = val;
                });
                return resource
                        .blob()
                        .then((blob) => {
                            console.log("(COMMIT_UNKNOWN) caching a Response to: " + key)
                            return cache.put(key, new Response(
                                blob,
                                init
                            ))
                        })
            }
        })
}

/**
 * remove resources from cache
 * 
 * implements the unstash() Samizdat plugin method 
 * 
 * accepts either a Response
 * or a string containing a URL
 * or an Array of string URLs
 */
let clearCachedContent = (resource) => {
    return caches.open('v1')
        .then((cache) => {
            if (typeof resource === 'string') {
                // assume URL
                console.log("(COMMIT_UNKNOWN) deleting a cached URL")
                return cache.delete(resource)
            } else if (Array.isArray(resource)) {
                // assume array of URLs
                console.log("(COMMIT_UNKNOWN) deleting an Array of cached URLs")
                return Promise.all(
                    resource.map((res)=>{
                        return cache.delete(res)
                    })
                )
            } else {
                // assume a Response
                // which means we have an URL in resource.url
                console.log("(COMMIT_UNKNOWN) removing a Response from cache: " + resource.url)
                return cache.delete(resource.url)
            }
        })
}

 
// initialize the SamizdatPlugins array
if (!Array.isArray(self.SamizdatPlugins)) {
    self.SamizdatPlugins = new Array()
}

// and add ourselves to it
// with some additional metadata
self.SamizdatPlugins.push({
    name: 'cache',
    description: 'Locally cached responses, using the Cache API.',
    version: 'COMMIT_UNKNOWN',
    fetch: getContentFromCache,
    stash: cacheContent,
    unstash: clearCachedContent
})
