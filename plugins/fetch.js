/* ========================================================================= *\
|* === Regular HTTP(S) fetch() plugin                                    === *|
\* ========================================================================= */

/**
 * this plugin does not implement any push method
 */

/**
 * getting content using regular HTTP(S) fetch()
 */
let fetchContent = (url) => {
    console.log('Samizdat: regular fetch!')
    return fetch(url, {cache: "reload"})
        .then((response) => {
            // 4xx? 5xx? that's a paddlin'
            if (response.status >= 400) {
                // throw an Error to fall back to Samizdat:
                throw new Error('HTTP Error: ' + response.status + ' ' + response.statusText);
            }
            // all good, it seems
            console.log("(COMMIT_UNKNOWN) Fetched:", response.url);
            
            // we need to create a new Response object
            // with all the headers added explicitly,
            // since response.headers is immutable
            var init = {
                status:     response.status,
                statusText: response.statusText,
                headers: {}
            };
            response.headers.forEach(function(val, header){
                init.headers[header] = val;
            });
            
            // add the X-Samizdat-* headers to the mix
            init.headers['X-Samizdat-Method'] = 'fetch'
            init.headers['X-Samizdat-ETag'] = response.headers.get('ETag')
            
            // return the new response, using the Blob from the original one
            return response
                    .blob()
                    .then((blob) => {
                        return new Response(
                            blob,
                            init
                        )
                    })
        })
}
 
// initialize the SamizdatPlugins array
if (!Array.isArray(self.SamizdatPlugins)) {
    self.SamizdatPlugins = new Array()
}

// and add ourselves to it
// with some additional metadata
self.SamizdatPlugins.push({
    name: 'fetch',
    description: 'Just a regular HTTP(S) fetch()',
    version: 'COMMIT_UNKNOWN',
    fetch: fetchContent,
})
